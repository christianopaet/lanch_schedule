<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');

            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');

            $table->string('lastname');
            $table->string('firstname');

            $table->integer('day');

            $table->integer('id_month');


            $table->integer('year');

            $table->string('sex');

            $table->string('height');
            $table->string('weight');

            $table->string('path');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info');
    }
}
