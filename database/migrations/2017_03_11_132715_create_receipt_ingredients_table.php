<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_ingredients', function (Blueprint $table){

           $table->increments('id');
           $table->integer('id_receipt')->unsigned();
           $table->integer('id_ingredient')->unsigned();

           $table->foreign('id_receipt')->references('id')->on('receipt')->onDelete('cascade');
           $table->foreign('id_ingredient')->references('id')->on('ingredient')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExist('receipt_ingredients');
    }
}
