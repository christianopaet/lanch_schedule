<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{

    protected $table = 'ingredient';

    public function receipt(){

        return $this->belongsToMany('App\Receipt');

    }

}
