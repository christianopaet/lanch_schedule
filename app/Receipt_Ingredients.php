<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt_Ingredients extends Model
{

    protected $table = 'receipt_ingredients';

    public function ingredient(){
        return $this->hasOne('App\Ingredient');
    }

}
