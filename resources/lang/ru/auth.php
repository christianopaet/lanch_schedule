<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'log_in' => 'Войти',
    'log_out' => 'Выйти',
    'register' => 'Зарегистрироватся',
    'password' => 'Пароль',
    'address' => 'Адрес',
    'remember_me' => 'Запомни меня',
    'password_forgot' => 'Забыли свой пароль',
    'login' => 'Логин',
    'confirm_password' => 'Подтвердите свой пароль',
    'my_account' => 'Мой кабинет'


];
