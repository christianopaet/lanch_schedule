<?php

return [

    'my_profile' => 'Мой профиль',
    'edit' => 'Редактировать',
    'my_ration' => 'Мой рацион',
    'favourite' => 'Избраные',
    'logout' => 'Выход',
    'male' => 'Мужской',
    'female' => 'Женский',
    'lastname' => 'Фамилия',
    'firstname' => 'Имя',
    'date' => 'День рождения',
    'sex' => 'Пол',
    'height' => 'Висота',
    'weight' => 'Вага',
    'path' => 'Выберите фото профиля',
    'submit' => 'Сохранить',
    'cropped_image' => 'Обрезаная фотография',
    'search' => 'Search',
    'search_ingredient' => 'Найдите ингредиенты',
    'enter_ingredients' => 'Введите свои ингредиенты',
    'choose_dish' => 'Выберите блюдо',
    'close' => 'Закрыть'

];