<?php

return [

    'newest_receipt' => 'Новый рецепт',
    'ingredients' => 'Ингредиенты',
    'choose_to_your_taste' => 'Выберите на свой вкус',
    'all_receipts' => 'Просмотреть все рецепты',
    'enter_your_cabinet' => 'Ввойдите в свой кабинет или',
    'register' => 'Зарегистрируйтесь',
    'all_rights_reserved' => 'Все права защищены',
    'copyright' => 'При копировании любых материалов активная ссылка на сайт обязательна',
    'used_products' => 'Используемые продукты',
    'preparing' => 'Приготовление',
    'calories' => 'Калории',
    'has_calories' => 'Это блюдо содержит <strong>:value</strong> калорий',
    'added_favourite' => ':value добавили',
    'upload' => 'Загрузить'

];