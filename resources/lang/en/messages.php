<?php

return [

    'newest_receipt' => 'New receipt',
    'ingredients' => 'Ingredients',
    'choose_to_your_taste' => 'Choose to your taste',
    'all_receipts' => 'Show all receipts',
    'enter_your_cabinet' => 'Enter to your own cabinet or',
    'register' => 'Register',
    'all_rights_reserved' => 'All rights reserved',
    'copyright' => 'When you copy any materials active link to the site is required',
    'used_products' => 'Products used',
    'preparing' => 'Preparing',
    'calories' => 'Calories',
    'has_calories' => 'This dish contains <strong>:value</strong> calories',
    'added_favourite' => ':value added'

];