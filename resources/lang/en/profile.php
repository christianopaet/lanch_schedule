<?php

return [

    'my_profile' => 'My profile',
    'edit' => 'Edit your profile',
    'my_ration' => 'My ration',
    'favourite' => 'Favourites',
    'logout' => 'Log out',
    'male' => 'Male',
    'female' => 'Female',
    'lastname' => 'Last name',
    'firstname' => 'First name',
    'date' => 'Date',
    'sex' => 'Sex',
    'height' => 'Height',
    'weight' => 'Weight',
    'path' => 'Choose your profile photo',
    'submit' => 'Save',
    'cropped_image' => 'Cropped Image',
    'upload' => 'Upload',
    'search' => 'Search',
    'search_ingredient' => 'Search for ingredient',
    'enter_ingredients' => 'Enter your ingredients',
    'choose_dish' => 'Choose dish',
    'close' => 'Close'

];