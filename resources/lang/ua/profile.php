<?php

return [

    'my_profile' => 'Мій профіль',
    'edit' => 'Редагувати',
    'my_ration' => 'Мій раціон',
    'favourite' => 'Обрані',
    'logout' => 'Вихід',
    'male' => 'Чоловіча',
    'female' => 'Жіноча',
    'lastname' => 'Прізвище',
    'firstname' => 'Ім\'я',
    'date' => 'Дата народження',
    'sex' => 'Стать',
    'height' => 'Висота',
    'weight' => 'Вага',
    'path' => 'Виберіть фото профіля',
    'submit' => 'Зберегти',
    'cropped_image' => 'Обрізана фотографія',
    'upload' => 'Завантажити',
    'search' => 'Search',
    'search_ingredient' => 'Найди інгредієнти',
    'enter_ingredients' => 'Введіть свої інгредієнти',
    'choose_dish' => 'Виберіть страву',
    'close' => 'Закрити'

];