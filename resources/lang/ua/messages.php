<?php

return [

    'newest_receipt' => 'Новий рецепт',
    'ingredients' => 'Інгредієнти',
    'choose_to_your_taste' => 'Виберіть на свій смак',
    'all_receipts' => 'Переглянути всі рецепти',
    'enter_your_cabinet' => 'Увійдіть у свій акаунт або',
    'register' => 'Зареєструйтеся',
    'all_rights_reserved' => 'Всі права захищені',
    'copyright' => 'При копіюванні будь-яких матеріалів активна ссилка на сайт обов\'язкова ',
    'used_products' => 'Використані продукти',
    'preparing' => 'Приготування',
    'calories' => 'Калорії',
    'has_calories' => 'Ця страва містить <strong>:value</strong> калорій',
    'added_favourite' => ':value добавили'

];