<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ці облікові дані не відповідають нашим записам.',
    'throttle' => 'Занадто багато спроб входу в систему. Будь ласка, спробуйте ще раз :seconds секунд.',

    'log_in' => 'Увійти',
    'log_out' => 'Вийти',
    'register' => 'Зареєструватися',
    'password' => 'Пароль',
    'address' => 'Адреса',
    'remember_me' => 'Запам\'ятай мене',
    'password_forgot' => 'Забули свій пароль',
    'login' => 'Логін',
    'confirm_password' => 'Підтвердіть свій пароль',
    'my_account' => 'Мій кабінет'

];
