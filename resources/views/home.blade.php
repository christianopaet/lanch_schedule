@extends('layouts/layout')

@section('body')

    <div class="container" ng-controller="homeCtrl">

        <div class="row home-top">

            <div class="col-md-8">

                <h3 class="category-title text-center">
                    <hr class="dotted-line" />
                    <span>@lang('messages.newest_receipt')</span>
                </h3>

                <a href="/<% locale %>/receipt/<% new_receipt.name %>" class="receipt-link">

                    <h4 class="receipt-title"><% new_receipt.name_trans  %></h4>

                    <img src="/images/receipts/<% new_receipt.path %>" alt="no-photo" class="img-responsive receipt-img img-rounded">

                </a>

            </div>
            <div class="col-md-4 nopadding">

                <h3 class="category-title text-center">
                    <hr class="dotted-line" />
                    <span>@lang('messages.choose_to_your_taste')</span>
                </h3>

                <div class="item-list">

                    <div ng-repeat="category in categories track by $index | limitTo: 10">

                        <a href="/<% locale %>/find/category/<% category.name %>" class="item-list-link">
                            <% category.name_trans %>
                        </a>

                    </div>

                </div>

            </div>

        </div>

        <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 receipt-block" ng-repeat="receipt in receipts |limitTo:12">

                <a href="/<% locale %>/receipt/<% receipt.name %>" class="receipt-link">

                    <h4 class="receipt-title receipt-title-small"><% receipt.name_trans  %></h4>

                    <img src="/images/receipts/<% receipt.path %>" alt="no-photo" class="img-responsive receipt-img img-rounded">

                </a>

            </div>

        </div>

        <div class="row all-receipts-button">

            <div class="col-md-12">

                <a href="/<% locale %>/receipts" class="btn btn-default pull-right">@lang('messages.all_receipts')</a>

            </div>

        </div>

    </div>

    @if (Auth::guest())

    <div class="container">

        <div class="row">

            <div class="login-in-title">

                <h3 class="text-center">@lang('messages.enter_your_cabinet') <a href="{{ route('register', ['locale' => Request::segment(1)]) }}">@lang('messages.register')</a></h3>
                <hr/>

            </div>

            <div class="log-in-content">

                <form role="form" method="POST" action="{{ route('login', ['locale' => $locale ]) }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-offset-2">
                        <label for="email" class="control-label"><span>E-Mail</span> @lang('auth.address')</label>

                        <div class="col-md-4">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="control-label">@lang('auth.password')</label>

                        <div class="col-md-2">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-4">
                            <button type="submit" class="btn">
                                @lang('auth.log_in')
                            </button>
                        </div>
                    </div>
                </form>

            </div>



        </div>

    </div>

    @endif

@endsection

@section('title', 'HomePage')

@section('script')

    <script src="/app/controllers/home.js"></script>

@endsection

@section('style')

    <link rel="stylesheet" href="/css/home.css">

@endsection
