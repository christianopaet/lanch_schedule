
@extends('layouts/layout')

@section('body')

    <div ng-app="foodApp">
        <div class="container" ng-controller="receiptCtrl">
            <div class="row">

                <div class="col-md-8">

                    <div class="article">

                        <div class="article-header">

                            <span><% receipt.name_trans %>
                                @if( !Auth::guest() )
                                    <input type="hidden" ng-init="user = {{ Auth::user()->id }}">
                                    <button class="favourite-star btn-link" ng-click="add_favourite('/api/post/add/favourite/' + receipt.id + '/{{ Auth::user()->id }}')" ng-class="{hidden: click}"><i class="fa fa-star-o"></i></button>
                                    <button class="favourite-star btn-link" ng-click="remove_favourite('/api/post/remove/favourite/' + receipt.id + '/{{ Auth::user()->id }}')" ng-class="{hidden : !click}"><i class="fa fa-star"></i></button>
                                @endif
                                <span class="favourite-added">@lang('messages.added_favourite', ['value' => '<% favourite_count %>'])</span>
                            </span>

                            <hr/>
                            <img src="/images/receipts/<% receipt.path %>" class="img-responsive" alt="Failed to load Photo">


                        </div>

                    </div>

                </div>

                <div class="col-md-4 ingredients">

                    <h3 class="ingredients-title text-center">
                        <hr class="dotted-line" />
                        <span>@lang('messages.used_products')</span>
                    </h3>

                    <div ng-repeat="value in ingredients">
                        <a title="<% value.name_trans %>" href="/<% locale %>/receipts/ingredient/<% value.name %>">
                            <img src="/images/ingredients/<% value.path %>" alt="Failed to load Photo">
                            <p><% value.name_trans|limitTo:15 %><span ng-if="value.name_trans.length > 15">...</span></p>
                        </a>
                    </div>

                    <div class="calories">

                        <span>@lang('messages.has_calories', ['value' => '<% calories|number: 2 %>'])</span>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-8">

                    <h3 class="ingredients-title text-center">
                        <hr class="dotted-line" />
                        <span>@lang('messages.ingredients')</span>
                    </h3>

                    <div class="panel">

                        <div class="panel-body">

                            <ul class="ingredients-list">
                                <li ng-repeat="value in ingredients">
                                    <% value.name_trans %> <strong><% value.quantity %> <% value.unit %></strong>
                                </li>
                            </ul>

                        </div>

                    </div>



                </div>

            </div>

            <div class="row">

                <div class="col-md-8">

                    <div class="preparing">

                        <h2>@lang('messages.preparing')</h2>

                        <p><% receipt.cooking %></p>

                    </div>

                </div>

            </div>

        </div>
    </div>




@endsection

@section('title', 'title')

@section('script')

    <script src="/app/controllers/receipt.js"></script>

@endsection

@section('style')

    <link rel="stylesheet" href="/css/receipt.css">

@endsection


