@extends('layouts/layout')

@section('body')

    <div class="container">

        <div class="row" ng-controller="searchCtrl">

            <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 receipt-block" ng-repeat="receipt in receipts">

                <a href="/<% locale %>/receipt/<% receipt.name %>" class="receipt-link">

                    <h4 class="receipt-title"><% receipt.name_trans  %></h4>

                    <img src="/images/receipts/<% receipt.path %>" alt="no-photo" class="img-responsive receipt-img img-rounded">

                </a>

            </div>

        </div>

    </div>

@endsection

@section('script')

    <script src="/app/controllers/search.js"></script>

@endsection

@section('style')

    <link rel="stylesheet" href="/css/receipts.css">

@endsection