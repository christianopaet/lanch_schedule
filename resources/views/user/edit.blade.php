@extends('layouts.profile')

@section('body')

    <div ng-controller="editCtrl" ng-init="user = {{ Auth::user()->id }}">

        <div class="loader" ng-show="is_active">
            <div class="circle">asd</div>
        </div>

        <div id="edit">

            {{--{{ Form::model($user, array('url' => '/api/post/add/user/info', 'method' => 'POST')) }}--}}

                {{--<div class="lastname">--}}
                    {{--{{ Form::label('lastname', __('profile.lastname')) }}--}}
                    {{--{{ Form::text('lastname', $user->lastname) }}--}}
                {{--</div>--}}

                {{--<div class="firstname">--}}
                    {{--{{ Form::label('firstname', __('profile.firstname')) }}--}}
                    {{--{{ Form::text('firstname', $user->firstname) }}--}}
                {{--</div>--}}

                {{--<div class="date">--}}
                    {{--{{ Form::label(__('profile.date')) }}--}}
                    {{--{{ Form::select('day', $day, $user->day) }}--}}
                    {{--{{ Form::select('month', $month, $user->month) }}--}}
                    {{--{{ Form::select('year', $year, $user->year) }}--}}
                {{--</div>--}}

                {{--<div class="sex">--}}
                    {{--{{ Form::label('sex', __('profile.sex')) }}--}}
                    {{--{{ Form::select('sex', array('m' => __('profile.male'), 'f' => __('profile.female')), $user->sex) }}--}}
                {{--</div>--}}

                {{--<div class="height">--}}
                    {{--{{ Form::label('height', __('profile.height')) }}--}}
                    {{--{{ Form::number('height', $user->height, array('min' => 0)) }}--}}
                {{--</div>--}}

                {{--<div class="weight">--}}
                    {{--{{ Form::label('weight', __('profile.weight')) }}--}}
                    {{--{{ Form::number('weight', $user->weight, array('min' => 0)) }}--}}
                {{--</div>--}}

                {{--<div class="image-upload">--}}
                    {{--{{ Form::label('path', __('profile.path')) }}--}}
                    {{--{{ Form::file('path', array('my-upload', 'id' => 'file_upload', 'class' => 'hidden')) }}--}}
                    {{--<button type="button" class="btn btn-default" uploadfile>Upload</button>--}}

                {{--</div>--}}

                {{--<div class="edit-submit">--}}

                    {{--{{ Form::submit(__('profile.submit')) }}--}}

                {{--</div>--}}

                {{--{{ Form::hidden('img_width', '<% area[0].width %>') }}--}}
                {{--{{ Form::hidden('img_height', '<% area[0].height %>') }}--}}
                {{--{{ Form::hidden('img_x', '<% area[0].x %>') }}--}}
                {{--{{ Form::hidden('img_y', '<% area[0].y %>') }}--}}
                {{--{{ Form::hidden('user_id', Auth::user()->id) }}--}}


            {{--{{ Form::close() }}--}}

            <div>

                <div class="lastname">

                    <label for="lastname">@lang('profile.lastname')</label>
                    <input type="text" class="has-error" ng-model="user_info.lastname" id="lastname">

                </div>

                <div class="firstname">

                    <label for="firstname">@lang('profile.firstname')</label>
                    <input type="text" ng-model="user_info.firstname" id="firstname">

                </div>

                <div class="date">

                    <label>@lang('profile.date')</label>
                    <select name="day" id="day" ng-model="day_selected" ng-options="item as item.label for item in day track by item.value"></select>
                    <select name="month" id="month" ng-model="month_selected" ng-options="item as item.label for item in month track by item.value"></select>
                    <select name="month" id="month" ng-model="year_selected" ng-options="item as item.label for item in year track by item.value"></select>

                </div>

                <div class="sex">

                    <label for="sex">@lang('profile.sex')</label>
                    <select name="sex" id="sex" ng-model="user_info.sex">
                        <option value="m">@lang('profile.male')</option>
                        <option value="f">@lang('profile.female')</option>
                    </select>

                </div>

                <div class="height">

                    <label for="height">@lang('profile.height')</label>
                    <input type="text" ng-model="user_info.height" id="height">

                </div>

                <div class="weight">

                    <label for="weight">@lang('profile.weight')</label>
                    <input type="text" ng-model="user_info.weight" id="weight">

                </div>

                <div class="image-upload">
                    <label>@lang('profile.path')</label>
                    <input type="file" id="fileInput" class="hidden">
                    <button type="button" class="btn btn-default" uploadfile>@lang('profile.upload')</button>
                </div>

                <div class="submit-button">

                    <button class="btn submit-btn" ng-click="save_data()">@lang('profile.submit')</button>

                </div>

            </div>

        </div>

        <div id="photo-view" ng-if="myImage.length != 0">

            <div class="close">

                <i class="fa fa-close" ng-click="resetImage()"></i>

            </div>

            <hr>

            <div class="cropArea">
                <img-crop image="myImage" result-image="myCroppedImage"></img-crop>
            </div>
            <div class="cropArea_1">@lang('profile.cropped_image'):</div>
            <div class="cropArea_1"><img class="img-circle" id="cropped-image" ng-src="<%myCroppedImage%>" /></div>

        </div>


    </div>

@endsection

@section('script')

    <script src="/app/controllers/edit.js"></script>

@endsection

@section('style')

    <link rel="stylesheet" href="/css/profile/edit.css">

@endsection