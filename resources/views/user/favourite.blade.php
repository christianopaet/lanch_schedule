@extends('layouts.profile')

@section('body')


    <div class="" ng-controller="favouritesCtrl" ng-init="user = {{ Auth::user()->id }}">

        <div class="col-md-4 receipt-block" ng-repeat="receipt in receipts">

            <a href="/<% locale %>/receipt/<% receipt.name %>" class="receipt-link">

                <h4 class="receipt-title"><% receipt.name_trans  %></h4>

                <img src="/images/receipts/<% receipt.path %>" alt="no-photo" class="img-responsive receipt-img img-rounded">

            </a>

        </div>

    </div>


@endsection

@section('script')

    <script src="/app/controllers/favourites.js"></script>

@endsection

@section('style')

    <link rel="stylesheet" href="/css/profile/favourite.css">

@endsection