@extends('layouts.profile')

@section('body')
    
    <div class="search" ng-controller="profileSearchCtrl">

        <div class="search-panel">

            <div class="header">

                <span>@lang('profile.enter_ingredients')</span>

            </div>

            <hr>

            <div class="selected-items">

                <div class="item" ng-repeat="item in selected_ingredients"><% item.name_trans %> <i ng-click="on_delete(item)" class="fa fa-close"></i></div>

            </div>

            <div class="input">

                <input placeholder="@lang('profile.search_ingredient')" ng-change="on_input()" ng-model="search" type="text" class="form-control">

            </div>

            <div class="items" ng-if="search.length > 0 && ingredients_search.length > 0">

                <div class="item" ng-repeat="item in ingredients_search" ng-click="on_add(item)"><% item.name_trans %> <i class="fa fa-plus pull-right"></i></div>

            </div>

        </div>

        <div class="search-result">

            <div class="header">

                Results:

            </div>

            <hr>

            <div class="result">

                <div class="header" ng-if="search_result.length == 0">

                    Not enough ingredients or no receipt

                </div>

                <a class="receipt" ng-repeat="item in search_result" href="/<% locale %>/receipt/<% item.name %>">

                    <img class="img-responsive" src="/images/receipts/<% item.path %>" alt="no-photo">

                    <div class="info">
                        <div class="name"><% item.name_trans %></div>
                        <div class="calories">Сalorie: <% get_calories(item) %></div>
                    </div>

                    <div class="ingredients">

                        <div class="icons">

                            <i class="fa fa-check-circle"></i>

                        </div>

                        <div class="enough">

                            <div class="item" ng-repeat="ingredient in item.has">

                                <img src="/images/ingredients/<% ingredient.path %>" class="img-responsive" alt="">

                                <p class="text-center"><% ingredient.name_trans %></p>

                            </div>

                        </div>

                        <div class="icons icon-x" ng-if="item.not.length != 0">

                            <i class="fa fa-close"></i>

                        </div>

                        <div class="not-enough">

                            <div class="item" ng-repeat="ingredient in item.not">

                                <img src="/images/ingredients/<% ingredient.path %>" alt="">

                                <p class="text-center"><% ingredient.name_trans %></p>

                            </div>

                        </div>

                    </div>

                </a>

            </div>

        </div>

    </div>
    
@endsection

@section('script')

    <script src="/app/controllers/profile_search.js"></script>

@endsection

@section('style')

    <link rel="stylesheet" href="/css/profile/search.css">

@endsection