@extends('layouts.profile')

@section('body')

    <div id="profile" ng-controller="profileCtrl" ng-init="user = {{ Auth::id() }}">

        <div class="photo">

            <img src="{{ asset('images/users/profile/medium/<% info.path %>') }}" class="img-circle" alt="no-photo">
            
        </div>
        
        <div class="user-name">

            <h3> <% info.lastname %> <% info.firstname %> </h3>

        </div>

    </div>

@endsection

@section('style')

    <link rel="stylesheet" href="{{ asset('/css/profile/profile_page.css') }}">

@endsection

@section('script')

    <script src="/app/controllers/profile.js"></script>

@endsection