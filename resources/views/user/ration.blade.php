@extends('layouts.profile')

@section('body')
     
    <!-- Trigger the modal with a button -->
    <button type="button" class="hidden" data-toggle="modal" data-target="#myModal" id="open-modal"></button>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" ng-controller="rationCtrl as vm">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">@lang('profile.choose_dish')</h4>
        </div>
        <div class="modal-body">
            <input type="text" ng-model="daySelected">
            <div class="receipts">
                <div class="scroll">
                    <div class="receipt" ng-repeat="item in receipts">
                        <div class="receipt-link" ng-click="addDish(item)">
                            <img src="/images/receipts/<% item.path %>" height="100" alt="">
                            <span><% item.name_trans %></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">@lang('profile.close')</button>
        </div>
        </div>

    </div>
    </div>
    
    <div id="calendar-block" ng-controller="rationCtrl">
    
        <table class="calendar-table">
            <thead>
                <tr class="thead">
                    <td></td>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                    <th>Sunday</th>
                </tr>
            </thead>

            <tbody class="" ng-repeat="item in calendar">
        
                <tr class="first-line tbody">
                    <th>
                        <% item.time %>:00
                    </th>
                    <td ng-click="openModal('monday', item.time, 0)"><% item.monday[0].name %></td>
                    <td ng-click="openModal('tuesday', item.time, 0)"><% item.tuesday[0].name %></td>
                    <td ng-click="openModal('wednesday', item.time, 0)"><% item.wednesday[0].name %></td>
                    <td ng-click="openModal('thursday', item.time, 0)"><% item.thursday[0].name %></td>
                    <td ng-click="openModal('friday', item.time, 0)"><% item.friday[0].name %></td>
                    <td ng-click="openModal('saturday', item.time, 0)"><% item.saturday[0].name %></td>
                    <td ng-click="openModal('sunday', item.time, 0)"><% item.sunday[0].name %></td>
                </tr>

                <tr class="second-line tbody">
                    <th>
                    </th>
                    <td ng-click="openModal('monday', item.time, 1)"><% item.monday[1].name %></td>
                    <td ng-click="openModal('tuesday', item.time, 1)"><% item.tuesday[1].name %></td>
                    <td ng-click="openModal('wednesday', item.time, 1)"><% item.wednesday[1].name %></td>
                    <td ng-click="openModal('thursday', item.time, 1)"><% item.thursday[1].name %></td>
                    <td ng-click="openModal('friday', item.time, 1)"><% item.friday[1].name %></td>
                    <td ng-click="openModal('saturday', item.time, 1)"><% item.saturday[1].name %></td>
                    <td ng-click="openModal('sunday', item.time, 1)"><% item.sunday[1].name %></td>
                </tr>
        
            </tbody>

        </table>

    </div>

@endsection

@section('style')

    <link rel="stylesheet" href="{{ asset('/css/profile/ration.css') }}">

@endsection

@section('script')

    <script src="/app/controllers/ration.js"></script>

@endsection