<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{ asset('css/profile/profile.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap.css') !!}">
    <link rel="stylesheet" href="{!! asset('resize/resize.css') !!}">

    @yield('style')

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body ng-app="foodApp">

        <div id="left-navbar" ng-controller="mainCtrl" ng-init="user = {{ Auth::user()->id }}">

            <div class="navbar-info">

                <img src="/images/users/profile/medium/<% user_info.path %>" width="50" height="50" alt="no-photo" class="img-circle">

                <div class="email">{{ Auth::user()->email }}</div>

            </div>

            <div class="navbar-items">

                <li class="first-element">
                    <div class="username"><% user_info.lastname %> <% user_info.firstname %></div>
                </li>

                <a href="/<% locale %>/user/profile" ng-class="{active: active === 'profile'}">
                    <li><i class="fa fa-home"></i> @lang('profile.my_profile')</li>
                </a>

                <a href="/<% locale %>/user/search" ng-class="{active: active === 'search'}">
                    <li><i class="fa fa-cutlery"></i> @lang('profile.search')</li>
                </a>

                <a href="/<% locale %>/user/edit" ng-class="{active: active === 'edit'}">
                    <li><i class="fa fa-edit"></i> @lang('profile.edit')</li>
                </a>

                <a href="/<% locale %>/user/ration" ng-class="{active: active === 'ration'}">
                    <li><i class="fa fa-clock-o"></i> @lang('profile.my_ration')</li>
                </a>

                <a href="/<% locale %>/user/favourite" ng-class="{active: active === 'favourite'}">
                    <li><i class="fa fa-star"></i> @lang('profile.favourite')</li>
                </a>

                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <li><i class="fa fa-sign-out"></i> @lang('profile.logout')</li>

                </a>
                <form id="logout-form" action="{{ route('logout', ['locale' => Request::segment(1)]) }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            </div>

            <div class="select-language">

                <a href="<% link_en %>"><img src="/images/flags/en.png" height="14px" alt="en"></a>
                <a href="<% link_ua %>"><img src="/images/flags/ua.png" height="14px" alt="ua"></a>
                <a href="<% link_ru %>"><img src="/images/flags/ru.png" height="14px" alt="ru"></a>

            </div>

        </div>

        <div id="content">

            @yield('body')

        </div>


<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="/app/lib/angular/angular.js"></script>
<script src="/app/lib/angular/modal.js"></script>
<script src="/js/jquery.js"></script>
<script src="/resize/resize.js"></script>
<script src="/app/app.js"></script>
<script src="/app/controllers/main.js"></script>

@yield('script')

</body>
</html>
