<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="locale" content="">

    <title>@yield('title')</title>

    <!-- Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">--}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('css/bootstrap.css') !!}">

    <!-- Styles -->

    <link rel="stylesheet" href="/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/css/layout.css">

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Open Sans', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }


    </style>

    @section('style')



    @show

</head>
<body ng-app="foodApp">

    <header>

        <div class="container-fluid" id="header-top">

            <div class="container">

                <div class="row">

                    <div class="col-md-3 pull-left" id="logo">

                        <a href="/{{ Request::segment(1) }}">
                            <span>Lunch Schedule</span>
                        </a>

                    </div>

                    <div class="pull-right" id="log-in-header">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <div id="my-cabinet">
                                <a href="{{ route('login', ['locale' => Request::segment(1)]) }}">@lang('auth.log_in')</a>
                                <a href="{{ route('register', ['locale' => Request::segment(1)]) }}">@lang('auth.register')</a>
                            </div>
                        @else

                            <div id="my-cabinet">
                                <a href="/{{  Request::segment(1) }}/user/profile"><span>@lang('auth.my_account')</span></a>

                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    @lang('auth.log_out')
                                </a>

                                <form id="logout-form" action="{{ route('logout', ['locale' => Request::segment(1)]) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>


                        @endif
                    </div>

                    {{--<div class="pull-right" id="icons-bar">--}}

                        {{--<div class="icons-title">--}}
                            {{--<span>Дізнайтеся про нас більше</span>--}}
                        {{--</div>--}}

                        {{--<div class="icons">--}}

                            {{--<a href="#">--}}
                                {{--<i class="fa fa-vk"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#">--}}
                                {{--<i class="fa fa-facebook"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#">--}}
                                {{--<i class="fa fa-instagram"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#">--}}
                                {{--<i class="fa fa-skype"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#">--}}
                                {{--<i class="fa fa-envelope"></i>--}}
                            {{--</a>--}}

                        {{--</div>--}}

                    {{--</div>--}}

                </div>

            </div>

        </div>

        <div class="container-fluid navbar">

            <div class="container">

                <div class="row" ng-controller="mainCtrl">

                    <div id="category-list">

                        <div ng-repeat="category in categories">

                        <a href="/<% locale %>/find/category/<% category.name %>" >

                            <li ng-if="category.father == 0">

                                <% category.name_trans %>

                            </li>

                        </a>

                        </div>

                        <div class="pull-right" id="search">

                            <!--action="<% '/' + locale + '/search/receipt/' + search %>" method="get"-->

                            <form>


                                <input type="text" name="search" ng-focus="focus=true;blur=false;" ng-blur="blur=true;focus=false;" ng-change="on_input()" ng-model="search" ng-value="search" class="form-control">

                                <a class="submit" href="<% '/' + locale + '/search/receipt/' + search %>">
                                    <i class="fa fa-search"></i>
                                </a>

                            </form>

                            <div class="search-result" ng-if="receipts_search.length != 0" ng-class="{'' : focus, hidden: blur }" >

                                <div ng-repeat="receipt in receipts_search| limitTo: 5">

                                    <a title="<% receipt.name_trans %>" href="/<% locale %>/receipt/<% receipt.name %>"><% receipt.name_trans| limitTo: 30 %><span ng-if="receipt.name_trans.length >= 30">...</span></a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </header>

    <main> 
    
        @section('body')


        @show
    
    </main>

    <footer>

        <div class="container-fluid" id="footer">

            <div class="container">
                <div class="row">

                    <div class="copy text-center">
                        &copy; @lang('messages.all_rights_reserved') назва.сайту
                    </div>

                    <div class="copy text-center">
                        @lang('messages.copyright')
                    </div>

                </div>
            </div>

        </div>

    </footer>

</body>

<script src="/app/lib/angular/angular.js"></script>
<script src="/app/lib/angular/modal.js"></script>
<script src="/js/jquery.js"></script>
<script src="/resize/resize.js"></script>
<script src="/app/app.js"></script>
<script src="/app/controllers/main.js"></script>
<script src="/js/effects.js"></script>

<script>
    window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
</script>

@section('script')

@show

</html>