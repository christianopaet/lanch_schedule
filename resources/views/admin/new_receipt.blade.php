@extends('layouts.admin')

@section('content')
    <div class="content" ng-controller="adminCtrl">

        {{ Form::open(array('url' => 'admin/add/new_receipt', 'method' => 'post')) }}

        <div id="new_receipt">

            <div>
                {{ Form::label('name', 'Name with no space') }}

                {{ Form::text('name') }}
            </div>

            <div>

                {{ Form::label('cooking', 'Receipt') }}

                {{ Form::textarea('cooking') }}

            </div>

            <div>

                {{ Form::label('path', 'Image name') }}

                {{ Form::text('path') }}

            </div>

            <div>

                {{ Form::label('id_category', 'Category') }}

                {{ Form::select('category', $categories) }}

            </div>

            <div>

                {{ Form::label('name_en', 'English name') }}

                {{ Form::text('name_en') }}

            </div>

            <div>

                {{ Form::label('name_ua', 'Ukrainian name') }}

                {{ Form::text('name_ua') }}

            </div>

            <div>

                {{ Form::label('name_ru', 'Russian name') }}

                {{ Form::text('name_ru') }}

            </div>

            <div ng-repeat="value in list_ingredients track by $index">

                {{ Form::select('ingredient_<% $index %>', $ingredients) }}

                {{ Form::text('quantity_<% $index %>') }}

                {{ Form::select('unit_<% $index %>', $units, null, ['placeholder' => 'Виберіть при необхідності']) }}

                <button type="button" class="btn btn-danger" ng-click="remove_ingredient($index)"><i class="fa fa-trash"></i></button>

            </div>

            <div>

                <button type="button" class="btn btn-success" ng-click="add_ingredient()"><i class="fa fa-plus"></i></button>

            </div>

            {{ Form::hidden('total', '<% total_ingredients %>') }}

            <div>

                {{ Form::submit('Add receipt') }}

            </div>

        </div>

        {{ Form::close() }}



    </div>

@endsection