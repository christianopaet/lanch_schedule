@extends('layouts.admin')

@section('content')

    <div id="ingredient-form" ng-controller="adminCtrl">

        {{ Form::open(array('url' => '/admin/new_ingredient', 'method' => 'POST')) }}
            <div class="col-md-4" ng-repeat="value in forms_ingredient track by $index">

                {{--<button class="btn btn-danger pull-right" ng-click="remove_form($index)"><i class="fa fa-trash"></i></button>--}}

                <div>

                    {{ Form::label('name_<% $index %>', 'Name with no space') }}
                    {{ Form::text('name_<% $index %>') }}

                </div>

                <div>

                    {{ Form::label('calories_<% $index %>', 'Calories') }}
                    {{ Form::number('calories_<% $index %>') }}

                </div>

                <div>

                    {{ Form::label('name_en_<% $index %>', 'English name') }}
                    {{ Form::text('name_en_<% $index %>') }}

                </div>

                <div>

                    {{ Form::label('name_ua_<% $index %>', 'Ukrainian name') }}
                    {{ Form::text('name_ua_<% $index %>') }}

                </div>

                <div>

                    {{ Form::label('name_ru_<% $index %>', 'Russian name') }}
                    {{ Form::text('name_ru_<% $index %>') }}

                </div>

                <div>

                    {{ Form::label('path_<% $index %>', 'Photo name') }}
                    {{ Form::text('path_<% $index %>') }}

                </div>

            </div>

            <div class="col-md-12">{{ Form::submit('Add') }}</div>

            {{ Form::hidden('total', '<% total_forms_ingredients %>') }}

        {{ Form::close() }}

        <div class="col-md-12">
            <button class="btn btn-success" ng-click="add_form()"><i class="fa fa-plus"></i></button>
        </div>



    </div>

@endsection