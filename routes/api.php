<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Ingredient;
use App\Receipt;
use App\Receipt_Ingredients;
use App\Categories;
use App\Units;
use App\Favourite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get/{locale}/receipt/{receipt_name}', function ($locale, $receipt_name){

    $receipt = Receipt::where('name', '=', $receipt_name)->first()->toArray();

    $result = [];

    array_push($result, ['id' => $receipt['id'], 'name' => $receipt['name'], 'name_trans' => $receipt['name_'.$locale], 'cooking' => $receipt['cooking'], 'path' => $receipt['path']]);

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/{locale}/receipt/ingredients/{receipt}', function ($locale, $receipt){

    $receipt_id = Receipt::where('name', '=', $receipt)->first()->id;


    $value = Receipt_Ingredients::where('id_receipt', '=', $receipt_id)->get()->toArray();

    $array = [];

    foreach ($value as $item) {

        $ingredient = Ingredient::find($item['id_ingredient']);

        $unit = Units::find($item['id_unit']);

        array_push($array, ['name' => $ingredient['name'] , 'path' => $ingredient['path'], 'quantity' => $item['quantity'], 'name_trans' => $ingredient['name_'.$locale], 'unit' => $unit['name_'.$locale]]);

    }

    return json_encode($array, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/{locale}/all/receipts', function ($locale){

    $array = Receipt::all()->toArray();

    $result = [];

    foreach ($array as $value) {

        array_push($result, ['id' => $value['id'], 'name' => $value['name'], 'name_trans' => $value['name_'.$locale], 'path' => $value['path']]);

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);
});

Route::get('/get/{locale}/categories', function ($locale){

    $array = Categories::all()->toArray();

    $result = [];

    foreach ($array as $value) {

        array_push($result, ['id' => $value['id'], 'name' => $value['name'], 'name_trans' => $value['name_'.$locale], 'father' => $value['father']]);

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/{locale}/find/category/{category}', function ($locale, $category){

    $result = [];

    $id_category = Categories::where('name', '=', $category)->first()->id;

    $receipts = Receipt::where('id_category', '=', $id_category)->get();

    foreach ($receipts as $receipt) {

        array_push($result, ['id' => $receipt['id'], 'name' => $receipt['name'], 'path' => $receipt['path'], 'name_trans' => $receipt['name_'.$locale]]);

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/{locale}/receipt/ingredient/{ingredient}', function ($locale, $ingredient){

    $result = [];

    $id_ingredient = Ingredient::where('name', '=', $ingredient)->first()->id;

    $receipts = Receipt_Ingredients::where('id_ingredient', '=', $id_ingredient)->get();

    foreach ($receipts as $receipt) {

        $temp = Receipt::find($receipt->id_receipt);

        array_push($result, ['id' => $temp['id'], 'name' => $temp['name'], 'path' => $temp['path'], 'name_trans' => $temp['name_'.$locale]]);

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/{locale}/search/receipt/{word}', function ($locale, $word){

    $result = [];

    $receipts = Receipt::where('name_'.$locale, 'ilike', '%' . $word . '%')->get();

    foreach ( $receipts as $receipt ) {

        array_push($result, ['id' => $receipt['id'], 'name' => $receipt['name'], 'path' => $receipt['path'], 'name_trans' => $receipt['name_'.$locale]]);

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::post('/post/add/favourite/{receipt}/{user}', function ($receipt, $user){

    $favourite = new Favourite();

    $result = Favourite::where('id_receipt', '=', $receipt)->where('id_user', '=', $user)->get();

    if ( count( $result ) == 0 ) {

        $favourite->id_receipt = $receipt;
        $favourite->id_user = $user;

        $favourite->save();

        return 1;

    }

    return 0;

});

Route::post('/post/remove/favourite/{receipt}/{user}', function ($receipt, $user){

    $result = Favourite::where('id_receipt', '=', $receipt)->where('id_user', '=', $user)->first();

    $result->delete();

    return 1;

});

Route::get('/get/find/favourite/{receipt}/{user}', function ($receipt, $user){

    $result = Favourite::where('id_receipt', '=', $receipt)->where('id_user', '=', $user)->first();

    return count( $result );

});

Route::get('/get/current/user', function (){

    if ( !Auth::guest() ){

        return json_encode( Auth::getFacadeApplication() );

    } else {

        return 0;

    }

});

Route::get('/get/receipt/calories/{receipt}', function ($receipt){

    $result = null;

    $receipt_id = Receipt::where('name', '=', $receipt)->first()->id;

    $ingredients = Receipt_Ingredients::where('id_receipt', '=', $receipt_id)->get();

    foreach ( $ingredients as $ingredient ) {

        if ( $ingredient->quantity != null ) {

            $temp = 0;

            switch ($ingredient->id_unit) {

                case 1:
                    $temp = 1;
                    break;
                case 2:
                    $temp = 1;
                    break;
                case 3:
                    $temp = 5;
                    break;
                case 4:
                    $temp = 40;
                    break;
                case 5:
                    $temp = 5;
                    break;


            }

            $result += $ingredient->quantity * $temp * Ingredient::find($ingredient->id_ingredient)->calories / 100;

        } else {

            $result += 10 * Ingredient::find($ingredient->id_ingredient)->calories / 100;

        }

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/count/receipt/favourite/{receipt_id}', function ($receipt_id){

    $result = Favourite::where('id_receipt', '=', $receipt_id)->get()->count();

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/{locale}/favourite/receipts/{user_id}', function ($locale, $user_id){

    $result = [];

    $receipts = \App\Favourite::where('id_user', '=', $user_id)->get();

    foreach ($receipts as $value) {

        $receipt = Receipt::find($value->id_receipt);

        array_push($result, ['id' => $receipt['id'], 'name' => $receipt['name'], 'path' => $receipt['path'], 'name_trans' => $receipt['name_'.$locale]]);

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::post('/post/add/user/info', function (Request $request){

    $array = \Illuminate\Support\Facades\Input::all();

    //return json_encode($array, JSON_UNESCAPED_UNICODE);

    if ($array['path']) {

        list($type, $array['path']) = explode(';', $array['path']);
        list(, $array['path'])      = explode(',', $array['path']);
        $file = base64_decode($array['path']);

        file_put_contents(public_path().'/images/users/profile/medium/'. $array['id_user'] . $array['lastname'] . $array['firstname'] .'.png', $file);

    }

    $user_info = \App\User_info::where('id_user', '=', $array['id_user'])->first();

    if(!$user_info) {
        $user_info = new \App\User_info();
    }

    $user_info->id_user = $array['id_user'];
    $user_info->lastname = $array['lastname'];
    $user_info->firstname = $array['firstname'];
    $user_info->day = $array['day'];
    $user_info->id_month = $array['id_month'];
    $user_info->year = $array['year'];
    $user_info->sex = $array['sex'];
    $user_info->height = $array['height'];
    $user_info->weight = $array['weight'];

    if (!$array['path'] && !$user_info->path) {

        $user_info->path = 'no_photo_'.$array['sex'].'.png';

    } else {

        $user_info->path = $array['id_user'] . $array['lastname'] . $array['firstname'] .'.png';

    }


    $user_info->save();

    //var_dump($user_info);

});

Route::get('/get/user/info/{user_id}', function ($user_id){

    $result = \App\User_info::where('id_user', '=', $user_id)->first();

    if (!$result) {

        $result = new \App\User_info();

        $result->id_user = $user_id;
        $result->day = 1;
        $result->id_month = 1;
        $result->year = 1970;
        $result->sex = 'm';

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/{locale}/month/created', function ($locale){

    $month = \App\Month::all()->toArray();

    $result = [];

    foreach ($month as $item) {

        array_push($result, ['value' => $item['id'], 'label' => $item['month_'.$locale]]);

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::get('/get/{locale}/all/ingredients', function ($locale){

    $result = [];

    $ingredients = Ingredient::all()->toArray();

    foreach ($ingredients as $ingredient) {

        array_push($result, ['id' => $ingredient['id'], 'name' => $ingredient['name'], 'name_trans' => $ingredient['name_'.$locale]]);

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

Route::post('/post/receipts/many/ingredients', function (){

    $array = \Illuminate\Support\Facades\Input::all();

    $locale = 'en';

    $receipts = Receipt::all()->toArray();

    $result = [];

    foreach ($receipts as $receipt) {

        $temp = get_test($array, $receipt['id'])[0];

        if ($temp['difference'] != 3) {

            $has =[];
            $not = [];

            foreach ($temp['has'] as $elem) {

                $ingredient = Ingredient::find($elem)->toArray();
                array_push($has, ['id' => $ingredient['id'], 'name' => $ingredient['name'], 'name_trans' => $ingredient['name_'.$locale], 'path' => $ingredient['path']]);

            }

            foreach ($temp['not'] as $elem) {

                $ingredient = Ingredient::find($elem->id_ingredient)->toArray();
                array_push($not, ['id' => $ingredient['id'], 'name' => $ingredient['name'], 'name_trans' => $ingredient['name_'.$locale], 'path' => $ingredient['path']]);

            }

            array_push($result, ['id' => $receipt['id'], 'name' => $receipt['name'], 'path' => $receipt['path'], 'name_trans' => $receipt['name_'.$locale], 'not_enough' => $temp['difference'], 'has' => $has, 'not' => $not]);

        }

    }

    return json_encode($result, JSON_UNESCAPED_UNICODE);

});

function get_test($array_ingredients, $id_receipt) {

    $key = 0;

    $have_ingredients = [];

    $receipt_ingredients = \App\Receipt_Ingredients::where('id_receipt', '=', $id_receipt)->get();

    foreach ($array_ingredients as $ingredient) {

        foreach ($receipt_ingredients as $receipt) {

            if ( $receipt->id_ingredient == $ingredient['id'] ) {

                array_push($have_ingredients, $receipt->id_ingredient);

                ++$key;
                break;

            }

        }


    }

    $not_ingredients = Receipt_Ingredients::where('id_receipt', '=', $id_receipt)->whereNotIn('id_ingredient', $have_ingredients)->get();

    if ( $key == count($receipt_ingredients) ) {

        $result = [];

        array_push($result, ['difference' => 0, 'has' => $have_ingredients, 'not' => $not_ingredients]);

        return $result;

    }

    if ( $key == count($receipt_ingredients) - 1 ) {

        $result = [];

        array_push($result, ['difference' => 1, 'has' => $have_ingredients, 'not' => $not_ingredients]);

        return $result;

    }

    if ( $key == count($receipt_ingredients) - 2 ) {

        $result = [];

        array_push($result, ['difference' => 2, 'has' => $have_ingredients, 'not' => $not_ingredients]);

        return $result;

    } else {

        $result = [];

        array_push($result, ['difference' => 3]);

        return $result;

    }

}
