<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Receipt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Ingredient;


Route::get('/{locale}/', function ($locale = 'ua') {

    if ($locale === 'login') {

        return  view('auth/login');

    }

    if ($locale === 'register') {

        return  view('auth/register');

    }

    if($locale !== 'ua' && $locale !== 'ru' && $locale !== 'en') {

        return view('404', ['locale' => 'ua']);

    }

    //setting locale

    App::setLocale($locale);

    //end

    return view('home', ['locale' => $locale]);
});

Route::get('/{locale}/receipt/{receipt}', function ($locale = 'ua', $receipt){

    //setting locale

    App::setLocale($locale);

    //end

    if( !Receipt::where('name', '=', $receipt) ){

        return redirect('/'.$locale.'/pages/404');

    }

    return view('receipt', ['locale' => $locale]);

});

Route::get('/{locale}/receipts', function ($locale){

    //setting locale

    App::setLocale($locale);

    //end

    return view('receipts', ['locale' => $locale]);

});

Route::get('/{locale}/find/category/{category}', function ($locale, $category){

    App::setLocale($locale);

    return view('category', ['locale' => $locale]);

});

Route::get('/{locale}/receipts/ingredient/{ingredient}', function ($locale, $ingredients){

    App::setLocale($locale);

    return view('receipts_ingredient', ['locale' => $locale]);

});

Route::get('/{locale}/search/receipt/{word}', function ($locale, $word){

    App::setLocale($locale);

    return view('search', ['locale' => $locale]);

});

Route::get('{locale}/pages/404', function ($locale){
    App::setLocale($locale);
    return view('404');

});

//Auth routes

Auth::routes();

//end


Route::group(['middleware' => ['auth']], function () {

    Route::get('/admin/new_receipt', function (){

        $user = Auth::user();

        if($user->name !== 'admin@domain.com' && $user->email !== 'admin@domain.com') {

            return redirect('en/pages/404');

        }

        $result = \App\Categories::all();

        $ingredients = Ingredient::pluck('name_ua', 'id');

        $units = \App\Units::pluck('name_ua', 'id');

        $categories = $result->pluck('name_ua', 'id');

//        foreach ($result as $value) {
//
//            array_push($categories, [$value->name_ua]);
//
//        }

        $receipt = new Receipt();

        return view('admin.new_receipt', ['receipt' => $receipt, 'categories' => $categories, 'ingredients' => $ingredients, 'units' => $units]);

    });

    Route::get('/admin/new_ingredient', function (){

        $user = Auth::user();

        if($user->name !== 'admin@domain.com' && $user->email !== 'admin@domain.com') {

            return redirect('en/pages/404');

        }



        return view('admin.new_ingredient');

    });

    Route::post('admin/new_ingredient', function (Request $request){

        $total = $request->input('total');

        for ($i = 0; $i < $total; ++$i) {

            $ingredient = new Ingredient();

            $ingredient->name = $request->input('name_'.$i);
            $ingredient->calories = $request->input('calories_'.$i);
            $ingredient->path = $request->input('path_'.$i);
            $ingredient->name_en = $request->input('name_en_'.$i);
            $ingredient->name_ua = $request->input('name_ua_'.$i);
            $ingredient->name_ru = $request->input('name_ru_'.$i);

            $ingredient->save();

        }

        return redirect('admin/new_ingredient');

    });

    Route::get('/admin/new_category', function (){

        $user = Auth::user();

        if($user->name !== 'admin@domain.com' && $user->email !== 'admin@domain.com') {

            return redirect('en/pages/404');

        }


        return view('admin.new_category');

    });

    Route::post('/admin/add/new_receipt', function (Request $request){

        $user = Auth::user();

        if($user->name !== 'admin@domain.com' && $user->email !== 'admin@domain.com') {

            return redirect('en/pages/404');

        }

        $receipt = new Receipt();

        $receipt->name = $request->input('name');
        $receipt->cooking = $request->input('cooking');
        $receipt->path = $request->input('path');
        $receipt->id_category = $request->input('category');
        $receipt->name_en = $request->input('name_en');
        $receipt->name_ua = $request->input('name_ua');
        $receipt->name_ru = $request->input('name_ru');

        $receipt->save();

        $total = $request->input('total');

        $receipt = Receipt::where('name', '=', $request->input('name'))->first();


        for ($i = 0; $i < $total; ++$i) {

            $receipt_ingredients = new \App\Receipt_Ingredients();

            $receipt_ingredients->id_receipt = $receipt->id;
            $receipt_ingredients->id_ingredient = $request->input('ingredient_'.$i);
            $receipt_ingredients->quantity = $request->input('quantity_'.$i);
            $receipt_ingredients->id_unit = $request->input('unit_'.$i);

            $receipt_ingredients->save();

        }

        return redirect('admin/new_receipt');

    });

    Route::get('/{locale}/user/profile', function ($locale){

        App::setLocale($locale);

        return view('user.profile');

    });

    Route::get('/{locale}/user/favourite', function ($locale){

        App::setLocale($locale);


        return view('user.favourite');

    });

    Route::get('/{locale}/user/edit', function ($locale){

        App::setLocale($locale);

        $user_id = Auth::id();

        $user = \App\User_info::where('id_user', '=', $user_id)->first();

        $month = \App\Month::pluck('month_'.$locale, 'id');

        $day = [];
        $year = [];

        for ( $i = 1; $i < 32; ++$i ) {

            $day[$i] = $i;

        }

        for ( $i = 1970; $i < date('Y'); ++$i ) {

            $year[$i] = $i;

        }

        if ( $user == null ) {

            $user = new \App\User_info();

        }

        return view('user.edit', ['user' => $user, 'day' => $day, 'month' => $month, 'year' => $year]);

    });

    Route::get('/{locale}/user/ration', function ($locale){

        App::setLocale($locale);


        return view('user.ration');

    });


    Route::get('/{locale}/user/search', function ($locale){

        App::setLocale($locale);


        return view('user.search');

    });


});

