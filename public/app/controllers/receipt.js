app.controller("receiptCtrl", ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.locale = Locale.locale;

    var pathName = window.location.toString();
    var receipt_name = pathName.split('/').pop(-1);


    $http.get('/api/get/' + $scope.locale + '/receipt/ingredients/' + receipt_name).success(function (data) {
        $scope.ingredients = data;
    }).error(function () {
        alert('Error');
    });

    $http.get('/api/get/' + $scope.locale + '/receipt/' + receipt_name).success(function (data) {
        $scope.receipt = data[0];
    }).error(function () {
        alert('Error');
    });

    $scope.ingredients = [];

    $scope.receipt = null;

    $scope.click = false;

    $scope.user = null;

    $scope.$watch('receipt', function () {

        $scope.$watch('user', function () {

            if ( $scope.user != null && $scope.receipt != null ) {

                $http.get('/api/get/find/favourite/' + $scope.receipt.id + '/' + $scope.user).success(function (data) {
                    if (data == 1){
                        $scope.click = true;
                    } else {
                        $scope.click = false;
                    }
                }).error(function () {
                    alert('Error!');
                });

            }

        });

    });

    $scope.calories = 0;

    $http.get( '/api/get/receipt/calories/' + receipt_name ).success(function (data) {
        $scope.calories = data;
    }).error(function () {
        alert('Error!');
    });

    $scope.favourite_count = 0;

    $scope.$watch('receipt', function () {

        if ($scope.receipt != null) {

            get_count_favourite();

        }

    });

    function get_count_favourite() {
        $http.get('/api/get/count/receipt/favourite/' + $scope.receipt.id).success(function (data) {
            $scope.favourite_count = data;
        }).error(function () {
            alert('Error!');
        });
    }




    $scope.add_favourite = function ($url) {
        $http.post($url).success(function () {
            $scope.click = true;
            get_count_favourite();
        }).error(function () {
            alert('Error');
        })
    };

    $scope.remove_favourite = function ($url) {
        $http.post($url).success(function () {
            $scope.click = false;
            get_count_favourite();
        }).error(function () {
            alert('Error');
        })
    };


}]);