app.controller('receiptsCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.receipts = [];

    $scope.locale = Locale.locale;

    $http.get('/api/get/' + $scope.locale + '/all/receipts').success(function (data) {
        $scope.receipts = data;
        console.log(data);
    }).error(function () {
        alert('Something goes wrong!');
    });

}]);