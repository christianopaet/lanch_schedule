app.controller('homeCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.locale = Locale.locale;

    $scope.categories = [];

    $http.get('/api/get/' + $scope.locale + '/categories').success(function (data) {
        $scope.categories = data;
        console.log(data);
    }).error(function () {
        alert('Somethings goes wrong');
    });

    $http.get('/api/get/' + $scope.locale + '/all/receipts').success(function (data) {
        $scope.new_receipt = data[data.length - 1];
        $scope.receipts = data;
    }).error(function () {
        alert('Somethings goes wrong');
    });

    $scope.new_receipt = [];
    $scope.receipts = [];

}]);