app.controller('profileSearchCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.locale = Locale.locale;

    $scope.selected_ingredients = [];

    $scope.ingredients = [];

    $scope.search_result = [];

    $http.get('/api/get/' + $scope.locale + '/all/ingredients').success(function (data) {

        $scope.ingredients = data;
        // $scope.selected_ingredients.push();

    }).error(function () {
        alert('Error!');
    });

    $scope.search = '';
    $scope.ingredients_search = [];

    $scope.on_input = function () {
        if( $scope.search.length == 0 ) {

            $scope.ingredients_search = [];

        } else {

            $scope.ingredients_search = [];

            angular.forEach($scope.ingredients, function (value, key) {

                if ( value.name_trans.toLowerCase().search($scope.search.toLowerCase()) >= 0 ) {

                    $scope.ingredients_search.push(value);

                }

            });

        }
    };

    $scope.on_add = function (item) {

        var select = $scope.ingredients.indexOf(item);
        $scope.selected_ingredients.push($scope.ingredients[select]);
        $scope.ingredients.splice(select, 1);
        $scope.on_input();

        $http.post('/api/post/receipts/many/ingredients', $scope.selected_ingredients).success(function (data) {
            $scope.search_result = data;
        }).error(function () {
            alert('Error!');
        });

    };

    $scope.get_calories = function (item) {

        var calories = 0;

        console.log(item);

        // $http.get( '/api/get/receipt/calories/' + item.name ).success(function (data) {
        //     console.log(data);
        // });

        return calories;

    };

    $scope.on_delete = function (item) {

        var select = $scope.selected_ingredients.indexOf(item);

        $scope.ingredients.push($scope.selected_ingredients[select]);

        $scope.selected_ingredients.splice(select, 1);

        $scope.on_input();

        $http.post('/api/post/receipts/many/ingredients', $scope.selected_ingredients).success(function (data) {
            $scope.search_result = data;
        }).error(function () {
            alert('Error!');
        });


    };



}]);