app.controller('receiptsIngredientCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.receipts = [];

    $scope.locale = Locale.locale;

    var pathname = window.location.toString();

    $scope.ingredient = pathname.split('/').pop(-1);

    console.log($scope.locale);

    $http.get('/api/get/' + $scope.locale + '/receipt/ingredient/' + $scope.ingredient).success(function (data) {

        console.log(data);

        $scope.receipts = data;

    }).error(function () {
        alert('Something goes wrong!');
    })

}]);