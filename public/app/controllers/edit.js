app.controller('editCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.image = "";

    $scope.user = null;

    $scope.locale = Locale.locale;

    $scope.user_info = {

        'id_user': $scope.user,
        'lastname': '',
        'firstname': '',
        'day': '1',
        'id_month': '1',
        'year': '1970',
        'sex': 'm'


    };

    $scope.area = [ {
        "x" : 100,
        "y" : 100,
        "height" : 100,
        "width" : 200,
        "name" : "Area",
        "id" : 1
    } ];

    $scope.day = [];
    $scope.day_selected = 0;

    $scope.month = [];
    $scope.month_selected = 0;

    $scope.year = [];
    $scope.year_selected = 0;

    $http.get('/api/get/' + $scope.locale + '/month/created').success(function (data) {
        $scope.month = data;
    }).error(function () {
        alert('Error!');
    });

    for( var i = 1; i < 32; ++i ) {

        $scope.day.push({'value' : i, 'label': i})


    }

    for( var j = 1970; j < new Date().getFullYear(); ++j ) {

        $scope.year.push({'value' : j, 'label': j})


    }

    $scope.$watch('user', function () {

        if( $scope.user != null ) {

            $http.get('/api/get/user/info/' + $scope.user).success(function (data) {
                $scope.user_info = data;

                $scope.day_selected = $scope.day[data.day - 1];
                $scope.month_selected = $scope.month[data.id_month - 1];
                $scope.year_selected = $scope.year[data.year - 1970];
                console.log(data);
            }).error(function () {
                alert('Error');
            })

        }

    });

    $scope.save_data = function () {

        $scope.user_info.day = $scope.day_selected['value'];
        $scope.user_info.id_month = $scope.month_selected['value'];
        $scope.user_info.year = $scope.year_selected['value'];
        if (document.querySelector('#cropped-image')) {
            $scope.user_info.path = document.querySelector('#cropped-image').attributes['src'].value;
        } else {

            $scope.user_info.path = '';

        }

        var data = $scope.user_info;


        if ( data.lastname == '' || data.firstname == '' || data.height == '' || data.weight == '' ){

            return false;

        }


        $http.post('/api/post/add/user/info', data).success(function () {
            alert('Saved');
        }).error(function () {
            alert('Error!');
        });

    };

    $scope.myImage = '';
    $scope.myCroppedImage = '';

    var handleFileSelect=function(evt) {
        var file=evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function($scope){
                $scope.myImage=evt.target.result;
            });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

    $scope.resetImage =function () {

        $scope.myImage = '';
        $scope.myCroppedImage = '';

    };



}]).directive('uploadfile', function () {
    return {
        restrict: 'A',
        link: function(scope, element) {

            element.bind('click', function(e) {
                angular.element(e.target).siblings('#fileInput').trigger('click');
            });
        }
    };
});