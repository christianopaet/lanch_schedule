app.controller('adminCtrl', ['$scope', function ($scope) {

    $scope.forms_ingredient = [
        1
    ];

    $scope.list_ingredients = [
        1
    ];

    $scope.total_ingredients = $scope.list_ingredients.length;

    $scope.add_ingredient = function () {
        $scope.list_ingredients.push(1);
        ++$scope.total_ingredients;
    };

    $scope.remove_ingredient = function (index) {
        $scope.list_ingredients.splice(index, 1);
        --$scope.total_ingredients;
    };

    $scope.add_form = function () {
        $scope.forms_ingredient.push(1);
        ++$scope.total_forms_ingredients;
    };
    
    $scope.remove_form = function (index) {

        // var index = $scope.forms.indexOf(item);
        $scope.forms_ingredient.splice(index, 1);
        --$scope.total_forms_ingredients;
    };

    // console.log($scope.list_ingredients);

    $scope.total_forms_ingredients = $scope.forms_ingredient.length;

}]);
