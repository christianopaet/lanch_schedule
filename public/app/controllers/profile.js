app.controller('profileCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.locale = Locale.locale;

    $scope.user = null;

    $scope.info = [];

    $scope.$watch('user', function () {

        if ($scope.user != null){

            $http.get('/api/get/user/info/' + $scope.user).success(function (data) {
                $scope.info = data;
                console.log(data);
            }).error(function () {
                alert('Error!');
            });

        }

    });

}]);