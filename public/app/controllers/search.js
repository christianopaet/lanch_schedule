app.controller('searchCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.receipts = [];

    $scope.locale = Locale.locale;

    var pathname = window.location.toString();

    $scope.word = pathname.split('/').pop(-1);


    $http.get('/api/get/' + $scope.locale + '/search/receipt/' + $scope.word).success(function (data) {

        $scope.receipts = data;

    }).error(function () {
        alert('Something goes wrong!');
    })

}]);