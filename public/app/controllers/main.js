app.controller('mainCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.categories = [];
    $scope.receipts = [];
    $scope.receipts_search = [];

    $scope.locale = Locale.locale;
    $scope.link_en = Locale.fullpath_en;
    $scope.link_ua = Locale.fullpath_ua;
    $scope.link_ru = Locale.fullpath_ru;

    $scope.user = null;
    $scope.user_info = [];

    $scope.$watch('user', function () {

        if($scope.user != null){

            $http.get('/api/get/user/info/' + $scope.user).success(function (data) {
                $scope.user_info = data;
            }).error(function () {
                alert('Error!');
            });

        }

    });

    $http.get('/api/get/' + $scope.locale + '/categories').success(function (data) {
        $scope.categories = data;
    }).error(function () {
        alert('Somethings goes wrong');
    });

    $http.get('/api/get/' + $scope.locale + '/all/receipts').success(function (data) {
        $scope.receipts = data;
    }).error(function () {
        alert('Somethings goes wrong');
    });

    $scope.search = '';

    $scope.on_input = function () {
        if( $scope.search.length <= 3 ) {

            $scope.receipts_search = [];

        } else {

            $scope.receipts_search = [];

            angular.forEach($scope.receipts, function (value, key) {

                if ( value.name_trans.toLowerCase().search($scope.search.toLowerCase()) >= 0 ) {

                    $scope.receipts_search.push(value);

                }

            });

        }
    };

    var path = window.location.toString();
    $scope.active = path.split('/').pop(-1);

    // $scope.change_class = function () {
    //     console.log(1);
    // };



}]);