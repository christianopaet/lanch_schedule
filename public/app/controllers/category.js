app.controller('categoryCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.receipts = [];

    $scope.locale = Locale.locale;

    var pathname = window.location.toString();

    $scope.category = pathname.split('/').pop(-1);

    console.log($scope.locale);

    $http.get('/api/get/' + $scope.locale + '/find/category/' + $scope.category).success(function (data) {

        $scope.receipts = data;

    }).error(function () {
        alert('Something goes wrong!');
    })

}]);