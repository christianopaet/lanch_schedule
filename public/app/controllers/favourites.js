app.controller('favouritesCtrl', ['$scope', '$http', 'Locale', function ($scope, $http, Locale) {

    $scope.receipts = [];

    $scope.locale = Locale.locale;

    $scope.user = null;

    $scope.$watch('user', function () {

        $http.get('/api/get/' + $scope.locale + '/favourite/receipts/' + $scope.user).success(function (data) {
            $scope.receipts = data;
        }).error(function () {
            alert('Error!');
        });

    });

}]);