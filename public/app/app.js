var app = angular.module("foodApp", ['ngImgCrop', 'angularModalService'], function ($interpolateProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});

app.factory('Locale', function () {

    var pathname = window.location.href;
    var temp = pathname.split("/")[3];

    var array = pathname.split('/');

    var fullpath_en = '/';
    var fullpath_ua = '/';
    var fullpath_ru = '/';

    angular.forEach(array, function (value, index) {

        if ( index > 3 ) {

            fullpath_en += value + '/';
            fullpath_ua += value + '/';
            fullpath_ru += value + '/';

        }

        if (index == 3) {

            fullpath_en += 'en/';
            fullpath_ua += 'ua/';
            fullpath_ru += 'ru/';

        }

    });

    return {
        locale: temp,
        fullpath_en: fullpath_en,
        fullpath_ua: fullpath_ua,
        fullpath_ru: fullpath_ru
    };

}).factory('loader', ['$rootScope', function($rootScope) {
    return {
        requests: [],
        add_request: function(url) {
            $rootScope.is_active = true;
            this.requests.push(url);
        },
        remove_request: function(url) {
            this.requests.splice(this.requests.indexOf(url), 1);
            if (this.requests.length == 0) {
                $rootScope.is_active = false;
            }
        },
    }
}]).factory('interceptor', ['loader', function(loader) {
    return {
        'request': function(request) {
            if (request.beforeSend) request.beforeSend();
            loader.add_request(request.url);
            return request;
        },
        'response': function(response) {
            loader.remove_request(response.config.url);
            if (response.config.complete) response.config.complete(response);
            return response;
        }
    };
}]);